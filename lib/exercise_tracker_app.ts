import { RestApi, LambdaIntegration, LambdaRestApi } from "aws-cdk-lib/aws-apigateway";
import { NodejsFunction } from "aws-cdk-lib/aws-lambda-nodejs";
import { Construct } from "constructs";
import { Bucket } from "aws-cdk-lib/aws-s3";

export class ExerciseTrackerAppStack extends Construct {
  constructor(scope: Construct, id: string) {
    super(scope, id);

    const bucket = new Bucket(this, "ExerciseTracker");
    
    const handler = new NodejsFunction(this, "ExerciseTrackerHandler", {
      environment: {
        BUCKET: bucket.bucketName
      }
    });

    bucket.grantReadWrite(handler);

    const api = new LambdaRestApi(this, "ExerciseTrackerApi", {
        handler,
        proxy: true,
        restApiName: "Exercise Tracker Service",
        description: "This service tracks exercises"
    });
  
  }
}