import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as exerciseTracker_service from '../lib/exercise_tracker_app';

export class ExerciseTrackerAppStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    new exerciseTracker_service.ExerciseTrackerAppStack(this, 'exercise');

  }
}
